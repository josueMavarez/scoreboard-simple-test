package scoreboardtest;

import java.io.BufferedOutputStream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JOptionPane;

public class server {

    private static ServerSocket serverSocket;
    private static Socket clientSocket;
    private static InputStreamReader inputStreamReader;
    private static BufferedReader bufferedReader;
    private static BufferedOutputStream bufferedOutputStream;
    private static OutputStreamWriter outputStreamWriter;
    private static String message;
    public String ipadd;
    public String ip;
    public controller control;
    public pantalla Controlador;

    public void run() {
        try {
            serverSocket = new ServerSocket(4444);
        } catch (IOException e) {
            System.out.println("Nope Port!");
        }

        while (true) {
            try {

                clientSocket = serverSocket.accept();

                ipadd = String.valueOf(clientSocket.getInetAddress());
                ipadd = ipadd.substring(1, (ipadd.length()));
                controller.ipAddress.setText("IP: " + ipadd);
                
                inputStreamReader = new InputStreamReader(clientSocket.getInputStream());
                bufferedReader = new BufferedReader(inputStreamReader);
                message = bufferedReader.readLine();
                
                switch (message) {
                    case "redUp":
                        controller.btnUpRed.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "redDown":
                        controller.btnDownRed.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "blueUp":
                        controller.btnUpBlue.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "blueDown":
                        controller.btnDownBlue.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "Connect":
                        controller.btnStart.doClick();
                        //controller.jLabel2.setText("");
                        break;
                    case "x1":
                        controller.tglByOne.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "x5":
                        controller.tglByFive.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "x10":
                        controller.tglByTen.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "x20":
                        controller.tglByTwenty.doClick();
                        controller.btnStart.doClick();
                        break;
                    case "x50":
                        controller.tglByFifty.doClick();
                        controller.btnStart.doClick();
                        break;
                }
                
                OutputStream os = clientSocket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);
                String send = controller.txtRedScore2.getText() + "-" +controller.txtBlueScore2.getText();
                System.out.println(send);
                bw.write(send);
                bw.flush();
                
                inputStreamReader.close();
                clientSocket.close();

            } catch (IOException ex) {
                System.out.println("Nope!!" + ex);
            }
        }
    }
}
