package scoreboardtest;

import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

public class pantalla extends javax.swing.JFrame {
    
    int py = 45;
    /*
    g4.drawImage(img, 0, 0, null);
        g4.drawImage(img2, 0, py, null);
        g4.drawImage(img3, 490, py, null);
        g4.drawImage(img4, 280, 35, null);
    lblBlueTeam
    lblRedTeam
    */
    
    public pantalla() {
        initComponents();
        this.setSize(800, 600);
        this.setResizable(false);
        showOnScreen(1,this);
        lblBlueTeam.setLocation(0, py);
        lblBlueTeam.setHorizontalAlignment(SwingConstants.CENTER);
        lblBlueTeam.setFont(new Font("Serif", Font.BOLD, 500));
        lblRedTeam.setLocation(490, py);
        lblRedTeam.setHorizontalAlignment(SwingConstants.CENTER);
        
        try {
            icon();
            } catch(IOException exc){
                JOptionPane.showMessageDialog(null, exc);
            }
    }
    
    public void icon() throws IOException {
        Image i = ImageIO.read(getClass().getResource("/Imagenes/thumbnailJosuee.png"));
        setIconImage(i);
    }//icon

    public static void actualizar(int redscore, int bluescore) throws InterruptedException {
        int scoreRojoActual = Integer.parseInt(txtRedScore.getText());
        int scoreAzulActual = Integer.parseInt(txtBlueScore.getText());
             
        txtRedScore.setText(""+redscore);
        txtBlueScore.setText(""+bluescore);
        
    }//actualizar
    
    
    public static void showOnScreen(int screen, JFrame frame){
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gd = ge.getScreenDevices();
       if(gd.length > 0){
            if(screen > -1 && screen < gd.length){
                frame.setLocation(gd[screen].getDefaultConfiguration().getBounds().x, frame.getY());
            } else if(gd.length > 0){
                frame.setLocation(gd[0].getDefaultConfiguration().getBounds().x, frame.getY());
            } else {
                throw new RuntimeException( "No Screens Found" );
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtLine = new javax.swing.JTextField();
        txtRedScore = new javax.swing.JTextField();
        txtBlueScore = new javax.swing.JTextField();
        lblJosuee = new javax.swing.JLabel();
        lblBlueTeam = new javax.swing.JLabel();
        lblRedTeam = new javax.swing.JLabel();
        txtLine2 = new javax.swing.JTextField();
        lblBackground = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("¡Batalla Epica Efesios!");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtLine.setFont(new java.awt.Font("Diogenes", 1, 100)); // NOI18N
        txtLine.setForeground(new java.awt.Color(255, 255, 255));
        txtLine.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtLine.setText("VS");
        txtLine.setAlignmentX(0.0F);
        txtLine.setBorder(null);
        txtLine.setFocusable(false);
        txtLine.setMaximumSize(new java.awt.Dimension(300, 300));
        txtLine.setMinimumSize(new java.awt.Dimension(300, 300));
        txtLine.setOpaque(false);
        txtLine.setPreferredSize(new java.awt.Dimension(300, 300));
        txtLine.setRequestFocusEnabled(false);
        txtLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLineActionPerformed(evt);
            }
        });
        getContentPane().add(txtLine, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 140, 250, 200));

        txtRedScore.setFont(new java.awt.Font("Diogenes", 1, 180)); // NOI18N
        txtRedScore.setForeground(new java.awt.Color(245, 28, 34));
        txtRedScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtRedScore.setText("0");
        txtRedScore.setAlignmentX(0.0F);
        txtRedScore.setBorder(null);
        txtRedScore.setFocusable(false);
        txtRedScore.setMaximumSize(new java.awt.Dimension(300, 300));
        txtRedScore.setMinimumSize(new java.awt.Dimension(300, 300));
        txtRedScore.setOpaque(false);
        txtRedScore.setRequestFocusEnabled(false);
        txtRedScore.setSelectedTextColor(new java.awt.Color(245, 28, 34));
        getContentPane().add(txtRedScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, 350, 200));

        txtBlueScore.setFont(new java.awt.Font("Diogenes", 1, 180)); // NOI18N
        txtBlueScore.setForeground(new java.awt.Color(105, 121, 193));
        txtBlueScore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtBlueScore.setText("0");
        txtBlueScore.setAlignmentX(0.0F);
        txtBlueScore.setBorder(null);
        txtBlueScore.setFocusable(false);
        txtBlueScore.setMaximumSize(new java.awt.Dimension(300, 300));
        txtBlueScore.setMinimumSize(new java.awt.Dimension(300, 300));
        txtBlueScore.setOpaque(false);
        txtBlueScore.setPreferredSize(new java.awt.Dimension(300, 300));
        txtBlueScore.setRequestFocusEnabled(false);
        txtBlueScore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBlueScoreActionPerformed(evt);
            }
        });
        getContentPane().add(txtBlueScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(435, 380, 360, 200));

        lblJosuee.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        //lblJosuee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/logo_blanco_opt2.png"))); // NOI18N
        lblJosuee.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblJosuee.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblJosuee.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(lblJosuee, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 15, 250, -1));

        lblBlueTeam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/azul.png"))); // NOI18N
        getContentPane().add(lblBlueTeam, new org.netbeans.lib.awtextra.AbsoluteConstraints(525, 40, 310, 310));

        lblRedTeam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/rojo.png"))); // NOI18N
        getContentPane().add(lblRedTeam, new org.netbeans.lib.awtextra.AbsoluteConstraints(-30, 60, 310, 290));

        txtLine2.setFont(new java.awt.Font("Diogenes", 1, 180)); // NOI18N
        txtLine2.setForeground(new java.awt.Color(255, 255, 255));
        txtLine2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtLine2.setText("-");
        txtLine2.setAlignmentX(0.0F);
        txtLine2.setBorder(null);
        txtLine2.setFocusable(false);
        txtLine2.setMaximumSize(new java.awt.Dimension(300, 300));
        txtLine2.setMinimumSize(new java.awt.Dimension(300, 300));
        txtLine2.setOpaque(false);
        txtLine2.setPreferredSize(new java.awt.Dimension(300, 300));
        txtLine2.setRequestFocusEnabled(false);
        getContentPane().add(txtLine2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 360, 250, 200));

        lblBackground.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/rockBG.jpg"))); // NOI18N
        lblBackground.setMaximumSize(new java.awt.Dimension(1280, 720));
        lblBackground.setMinimumSize(new java.awt.Dimension(1280, 720));
        lblBackground.setPreferredSize(new java.awt.Dimension(800, 600));
        getContentPane().add(lblBackground, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, -10, 850, 630));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBlueScoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBlueScoreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBlueScoreActionPerformed

    private void txtLineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLineActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLineActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pantalla().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblBackground;
    private javax.swing.JLabel lblBlueTeam;
    private javax.swing.JLabel lblJosuee;
    private javax.swing.JLabel lblRedTeam;
    public static volatile javax.swing.JTextField txtBlueScore;
    public static javax.swing.JTextField txtLine;
    public static javax.swing.JTextField txtLine2;
    public static volatile javax.swing.JTextField txtRedScore;
    // End of variables declaration//GEN-END:variables
}
