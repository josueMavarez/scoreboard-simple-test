/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scoreboardtest;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class controller extends javax.swing.JFrame {

    public static int Multiplier = 1;
    public pantalla Controlador;
    public server svr;

    public controller() {
        initComponents();
        this.setLocationRelativeTo(this);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tglByOne.setSelected(true);
        try {
            icon();
        } catch (IOException exc) {
            JOptionPane.showMessageDialog(null, "Error Icono");
        }
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        //int height = (int) screenSize.getHeight();

        //maraña NVM
        Controlador = new pantalla();
        Controlador.dispose();
        Controlador.setLocation(width, 0);
        Controlador.setUndecorated(true);
        Controlador.setVisible(true);
        //maraña NVM
        new Thread(new Runnable() {
            @Override
            public void run() {
                server svr = new server();
                svr.run();
            }
        }).start();
        //maraña NVM Like a pro
        //System.out.println("hola");

    }

    public void icon() throws IOException {
        Image i = ImageIO.read(getClass().getResource("/Imagenes/thumbnailJosuee.png"));
        setIconImage(i);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpMultiplier = new javax.swing.ButtonGroup();
        btnStart = new javax.swing.JButton();
        lblRedTeam = new javax.swing.JLabel();
        lblBlueTeam = new javax.swing.JLabel();
        lblMultiplier = new javax.swing.JLabel();
        txtRedScore2 = new javax.swing.JTextField();
        txtBlueScore2 = new javax.swing.JTextField();
        tglByOne = new javax.swing.JToggleButton();
        tglByFive = new javax.swing.JToggleButton();
        tglByTen = new javax.swing.JToggleButton();
        tglByTwenty = new javax.swing.JToggleButton();
        tglByFifty = new javax.swing.JToggleButton();
        tglCustom = new javax.swing.JToggleButton();
        txtCustom = new javax.swing.JTextField();
        btnDownRed = new javax.swing.JButton();
        btnUpRed = new javax.swing.JButton();
        btnUpBlue = new javax.swing.JButton();
        btnDownBlue = new javax.swing.JButton();
        ipAddress = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SomosJOSUEE V.0.1");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnStart.setFont(new java.awt.Font("Diogenes", 0, 18)); // NOI18N
        btnStart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/startBtn.png"))); // NOI18N
        btnStart.setText("Update");
        btnStart.setToolTipText("");
        btnStart.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnStart.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnStart.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnStart.setOpaque(false);
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });
        getContentPane().add(btnStart, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 340, 40));

        lblRedTeam.setFont(new java.awt.Font("Bebas Neue", 0, 18)); // NOI18N
        lblRedTeam.setForeground(new java.awt.Color(102, 0, 0));
        lblRedTeam.setText("Equipo Rojo: ");
        getContentPane().add(lblRedTeam, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, 40));

        lblBlueTeam.setFont(new java.awt.Font("Bebas Neue", 0, 18)); // NOI18N
        lblBlueTeam.setForeground(new java.awt.Color(0, 0, 102));
        lblBlueTeam.setText("Equipo Azul: ");
        getContentPane().add(lblBlueTeam, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 180, 40));

        lblMultiplier.setFont(new java.awt.Font("Bebas Neue", 0, 18)); // NOI18N
        lblMultiplier.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMultiplier.setText("Multiplier");
        getContentPane().add(lblMultiplier, new org.netbeans.lib.awtextra.AbsoluteConstraints(267, 10, 110, -1));

        txtRedScore2.setFont(new java.awt.Font("Bebas Neue", 0, 36)); // NOI18N
        txtRedScore2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtRedScore2.setText("0");
        txtRedScore2.setBorder(null);
        txtRedScore2.setOpaque(false);
        txtRedScore2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRedScore2KeyTyped(evt);
            }
        });
        getContentPane().add(txtRedScore2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 20, 60, 40));

        txtBlueScore2.setFont(new java.awt.Font("Bebas Neue", 0, 36)); // NOI18N
        txtBlueScore2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtBlueScore2.setText("0");
        txtBlueScore2.setBorder(null);
        txtBlueScore2.setOpaque(false);
        txtBlueScore2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBlueScore2KeyTyped(evt);
            }
        });
        getContentPane().add(txtBlueScore2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 90, 60, 40));

        grpMultiplier.add(tglByOne);
        tglByOne.setFont(new java.awt.Font("Diogenes", 0, 10)); // NOI18N
        tglByOne.setText("x1");
        tglByOne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglByOneActionPerformed(evt);
            }
        });
        getContentPane().add(tglByOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, 50, -1));

        grpMultiplier.add(tglByFive);
        tglByFive.setFont(new java.awt.Font("Diogenes", 0, 10)); // NOI18N
        tglByFive.setText("x5");
        tglByFive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglByFiveActionPerformed(evt);
            }
        });
        getContentPane().add(tglByFive, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 40, 50, -1));

        grpMultiplier.add(tglByTen);
        tglByTen.setFont(new java.awt.Font("Diogenes", 0, 10)); // NOI18N
        tglByTen.setText("x10");
        tglByTen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglByTenActionPerformed(evt);
            }
        });
        getContentPane().add(tglByTen, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 70, -1, -1));

        grpMultiplier.add(tglByTwenty);
        tglByTwenty.setFont(new java.awt.Font("Diogenes", 0, 10)); // NOI18N
        tglByTwenty.setText("x20");
        tglByTwenty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglByTwentyActionPerformed(evt);
            }
        });
        getContentPane().add(tglByTwenty, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 70, -1, -1));

        grpMultiplier.add(tglByFifty);
        tglByFifty.setFont(new java.awt.Font("Diogenes", 0, 10)); // NOI18N
        tglByFifty.setText("x50");
        tglByFifty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglByFiftyActionPerformed(evt);
            }
        });
        getContentPane().add(tglByFifty, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 100, -1, -1));

        grpMultiplier.add(tglCustom);
        tglCustom.setFont(new java.awt.Font("Diogenes", 0, 10)); // NOI18N
        tglCustom.setText("##");
        tglCustom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglCustomActionPerformed(evt);
            }
        });
        getContentPane().add(tglCustom, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 100, 50, -1));

        txtCustom.setEditable(false);
        txtCustom.setFont(new java.awt.Font("Diogenes", 0, 10)); // NOI18N
        txtCustom.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCustom.setText("0");
        txtCustom.setToolTipText("");
        txtCustom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCustomKeyTyped(evt);
            }
        });
        getContentPane().add(txtCustom, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 130, 110, -1));

        btnDownRed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/arrowDown.png"))); // NOI18N
        btnDownRed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownRedActionPerformed(evt);
            }
        });
        getContentPane().add(btnDownRed, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 20, 20));

        btnUpRed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/arrowUp.png"))); // NOI18N
        btnUpRed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpRedActionPerformed(evt);
            }
        });
        getContentPane().add(btnUpRed, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 20, 20, 20));

        btnUpBlue.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/arrowUp.png"))); // NOI18N
        btnUpBlue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpBlueActionPerformed(evt);
            }
        });
        getContentPane().add(btnUpBlue, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 90, 20, 20));

        btnDownBlue.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/arrowDown.png"))); // NOI18N
        btnDownBlue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownBlueActionPerformed(evt);
            }
        });
        getContentPane().add(btnDownBlue, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 110, 20, 20));

        ipAddress.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        getContentPane().add(ipAddress, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 220, -1));

        jLabel1.setText("IP CONNECTED: ");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
        int scoreRed = Integer.parseInt(txtRedScore2.getText());
        int scoreBlue = Integer.parseInt(txtBlueScore2.getText());

        try {
            Controlador.actualizar(scoreRed, scoreBlue);
        } catch (InterruptedException ex) {
            Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_btnStartActionPerformed

    private void txtRedScore2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRedScore2KeyTyped
        char input = evt.getKeyChar();

        if (txtRedScore2.getText().length() > 1 || !Character.isDigit(input)) {
            evt.consume();
        }
    }//GEN-LAST:event_txtRedScore2KeyTyped

    private void txtBlueScore2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBlueScore2KeyTyped
        char input = evt.getKeyChar();

        if (txtBlueScore2.getText().length() > 1 || !Character.isDigit(input)) {
            evt.consume();
        }
    }//GEN-LAST:event_txtBlueScore2KeyTyped

    private void btnDownRedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownRedActionPerformed
        Integer redScore = Integer.parseInt(txtRedScore2.getText());
        if (redScore >= 1) {
            if (tglCustom.isSelected()) {
                Multiplier = Integer.parseInt(txtCustom.getText());
            }//if
            redScore -= Multiplier;
            txtRedScore2.setText("" + redScore);
        }//if
    }//GEN-LAST:event_btnDownRedActionPerformed

    private void btnUpRedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpRedActionPerformed
        Integer redScore = Integer.parseInt(txtRedScore2.getText());
        if (tglCustom.isSelected()) {
            Multiplier = Integer.parseInt(txtCustom.getText());
        }//if
        redScore += Multiplier;
        txtRedScore2.setText("" + redScore);
    }//GEN-LAST:event_btnUpRedActionPerformed

    private void btnUpBlueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpBlueActionPerformed
        Integer blueScore = Integer.parseInt(txtBlueScore2.getText());
        if (tglCustom.isSelected()) {
            Multiplier = Integer.parseInt(txtCustom.getText());
        }//if
        blueScore += Multiplier;
        txtBlueScore2.setText("" + blueScore);
    }//GEN-LAST:event_btnUpBlueActionPerformed

    private void btnDownBlueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownBlueActionPerformed
        Integer blueScore = Integer.parseInt(txtBlueScore2.getText());
        if (blueScore >= 1) {
            if (tglCustom.isSelected()) {
                Multiplier = Integer.parseInt(txtCustom.getText());
            }//if
            blueScore -= Multiplier;
            txtBlueScore2.setText("" + blueScore);
        }//if
    }//GEN-LAST:event_btnDownBlueActionPerformed

    private void txtCustomKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCustomKeyTyped
        char input = evt.getKeyChar();

        if (txtCustom.getText().length() > 2 || !Character.isDigit(input)) {
            evt.consume();
        }
        if (txtCustom.getText().length() == 0) {
            txtCustom.setText("0");
        }//if
    }//GEN-LAST:event_txtCustomKeyTyped

    private void tglCustomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglCustomActionPerformed
        if (tglCustom.isSelected()) {
            txtCustom.setEditable(true);
        }//if
    }//GEN-LAST:event_tglCustomActionPerformed

    private void tglByOneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglByOneActionPerformed
        Multiplier = 1;
        txtCustom.setEditable(false);
    }//GEN-LAST:event_tglByOneActionPerformed

    private void tglByFiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglByFiveActionPerformed
        Multiplier = 5;
        txtCustom.setEditable(false);
    }//GEN-LAST:event_tglByFiveActionPerformed

    private void tglByTenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglByTenActionPerformed
        Multiplier = 10;
        txtCustom.setEditable(false);
    }//GEN-LAST:event_tglByTenActionPerformed

    private void tglByTwentyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglByTwentyActionPerformed
        Multiplier = 20;
        txtCustom.setEditable(false);
    }//GEN-LAST:event_tglByTwentyActionPerformed

    private void tglByFiftyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglByFiftyActionPerformed
        Multiplier = 50;
        txtCustom.setEditable(false);
    }//GEN-LAST:event_tglByFiftyActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new RunnableImpl());
    }

    public static void showOnScreen(int screen, JFrame frame) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gd = ge.getScreenDevices();
        if (gd.length > 0) {
            if (screen > -1 && screen < gd.length) {
                frame.setLocation(gd[screen].getDefaultConfiguration().getBounds().x, frame.getY());
            } else if (gd.length > 0) {
                frame.setLocation(gd[0].getDefaultConfiguration().getBounds().x, frame.getY());
            } else {
                throw new RuntimeException("No Screens Found");
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnDownBlue;
    public static javax.swing.JButton btnDownRed;
    public static javax.swing.JButton btnStart;
    public static javax.swing.JButton btnUpBlue;
    public static javax.swing.JButton btnUpRed;
    private javax.swing.ButtonGroup grpMultiplier;
    public static javax.swing.JLabel ipAddress;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblBlueTeam;
    private javax.swing.JLabel lblMultiplier;
    private javax.swing.JLabel lblRedTeam;
    public static javax.swing.JToggleButton tglByFifty;
    public static javax.swing.JToggleButton tglByFive;
    public static javax.swing.JToggleButton tglByOne;
    public static javax.swing.JToggleButton tglByTen;
    public static javax.swing.JToggleButton tglByTwenty;
    public static javax.swing.JToggleButton tglCustom;
    public static javax.swing.JTextField txtBlueScore2;
    public static javax.swing.JTextField txtCustom;
    public static javax.swing.JTextField txtRedScore2;
    // End of variables declaration//GEN-END:variables

    private static class RunnableImpl implements Runnable {

        public RunnableImpl() {
        }

        @Override
        public void run() {
            new controller().setVisible(true);
        }
    }
}
